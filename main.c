
#include <stdio.h>
#include <math.h>
 
volatile float Test_Sin=0.0f;
volatile float Test_Cos=0.0f;
volatile float Test_angle=0.0f;
volatile float Test_gain=1.0f;
volatile unsigned Time=0;

void Init()
{

}

void Loop()
{
	Time++;
	Test_Sin = Test_gain*sin(Test_angle); 
	Test_Cos = Test_gain*cos(Test_angle);
	Test_angle +=0.01;
}


/* ---- Entrypoint -----*/
int main()
{

	Init();

    while(1)
    {
    	Loop(); 
    }
	return 0;
}
