# DUT

DUT - simulated Device under Test .

## Usage

This tool is meant to be used with the SignalPlot mainly for testing purposes.
(See https://gitlab.com/VladVasiliu/SignalPlot.git)

This is basically a template for a "Device under Test" . It is a minimal integration context siminar with an  Arduino project .
You can integrate your SW modules like in Arduino (do not use the main function).

All the global variables can be monitored  using the SignalPlot 
